# Лабораторные работы по курсу СТРПО


Семестровое задание:

Цель: разработать многопоточный обработчик изображений с применением фильтров, визуально похожих на Instagram (далее система)

Техническое задание:
1. Язык разработки - любой
2. Клиент системы - любой
3. Система должна принимать на вход изображение и вектор параметров, характеризующих настройки применяемого фильтра
4. Многопоточная обработка должна минимизировать время от запроса обработки до выдачи обработанного изображения
5. Система должна иметь возможность масштабироваться по количеству доступных ядер
6. Система должна обеспечивать паралельное применение фильтров для изображений большого объема
